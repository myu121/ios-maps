//
//  MyPoints.swift
//  FileManager
//
//  Created by Michael Yu on 4/22/21.
//

import Foundation
import MapKit

class MyPoints: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var locationName: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D){
        self.coordinate = coordinate
        super.init()
    }
}
