//
//  AppError.swift
//  F1
//
//  Created by Michael Yu on 4/5/21.
//

import Foundation

enum AppError: Error {
    case badUrl
    case serverError
    case badResponse
    case noData
    case parseError
    case genericError(String)
    
    var errorMessage: String {
        switch self {
        case .badUrl:
            return "URL is not valid"
        case .serverError:
            return "Server Error"
        case .badResponse:
            return "Bad HTTP response"
        case .noData:
            return "No data found"
        case .parseError:
            return "Parsing Error"
        case .genericError(let message):
            return message
        }
    }
}

enum MapError: Error {
    case emptySearchField
}
