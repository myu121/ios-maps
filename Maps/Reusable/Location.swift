//
//  Location.swift
//  FileManager
//
//  Created by Michael Yu on 4/20/21.
//

import CoreLocation
import Foundation
import UIKit

protocol LocationDelegate: AnyObject {
    func getLocation(location: CLLocation) -> CLLocation
}

class Location: NSObject {
    var locationManager = CLLocationManager() {
        didSet {
            locationManager.delegate = self
        }
    }
    
    weak var delegate: LocationDelegate?
    private weak var controller: UIViewController?
    var geocoder = CLGeocoder()
    
    init(presentingController controller: UIViewController) {
        self.controller = controller
        super.init()
    }
    
    private func requestAuthorization(){
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
    }
    
    func checkAuthorizationAndAccessLocation() {
        let status: CLAuthorizationStatus
        
        if #available(iOS 14.0, *) {
            status = locationManager.authorizationStatus
        } else {
            status = CLLocationManager.authorizationStatus()
        }
        
        switch status {
        case .denied, .restricted:
            displayLocationAccessAlert()
            break
        case .notDetermined:
            requestAuthorization()
            
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        // locationManager.requestLocation()
        
        @unknown default:
            break
        }
    }
    
    private func displayLocationAccessAlert() {
        self.controller?.showAlert(title: "Alert", message: "Allow location access from settings", buttons: [.ok]) { (alert, button) in
            
        }
    }
    
    func geocode(address: String, completion: @escaping (CLPlacemark) -> ()) {
        geocoder.geocodeAddressString(address) { (placemark, error) in
            if let placemark = placemark?[0] {
                completion(placemark)
            }
        }
    }
    
    func reverseGeocode(placemark: CLPlacemark, completion: @escaping (String) -> ()) {
        var address = ""
        geocoder.reverseGeocodeLocation(placemark.location ?? CLLocation()) { (placemark, error) in
            guard let placemark = placemark?[0] else { return }
            address += placemark.name ?? ""
            address += ", "
            address += placemark.locality ?? ""
            address += ", "
            address += placemark.administrativeArea ?? ""
            address += " "
            address += placemark.postalCode ?? ""
            completion(address)
        }
    }
}

extension Location: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
        //        guard let currentLocation = locations.last else { return }
        //        delegate?.getLocation(location: currentLocation)
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    }
}
