# iOS Maps Demo
This is a demo application showcasing an implementation of 2 maps using Apple's MapKit framework and Google's Map framework.

## Apple Maps

Local search functionality based on search query

<img src="https://media.giphy.com/media/ChqmBbLx5wRrFpICwb/giphy.gif">

## Google Maps

Search functionality based on Google Places API

<img src="https://media.giphy.com/media/wXUEi86knSBzkBfhMP/giphy.gif">

## API Reference

https://developers.google.com/maps/documentation/places/web-service/overview

## Technologies

iOS 13.0 or above

Xcode 12.5

Swift 5

## Frameworks

UIKit

MapKit

GoogleMaps

GooglePlaces

## Supported Devices

iPhone SE (2nd gen)

iPhone 8 - 12 (All sizes supported)